
import './styles/app.scss';
import './styles/boutique.css';
import './styles/footer.css';
import './styles/carousel.css';

const $ = require('jquery');

require('bootstrap');
import './jquery.instantSearch.js';
$('.deleteBtn').on('click', function () {
    let removeUrl = $(this).attr('data-remove-url');
    let id = $(this).attr('data-id');

    $('.remove_item').attr('data-remove-url', removeUrl);
    $('.remove_item').attr('data-id', id);
});

$('.remove_item').on('click', function (e) {
    let removeUrl = $(this).attr('data-remove-url');
    let id = $(this).attr('data-id');

    $.ajax({
        url: removeUrl,
        type: 'POST',
        data: {},
        contentType: 'text',
        success: function (data) {

            $('#address' + id).remove();
            $('div.modal-content').html(data);

            setTimeout(function () {
                $('#confirmationModal').modal('hide');
            }, 5000);
        },
        error: function (jqXHR) {
            $('div.modal-content').html(jqXHR.responseText)
        }
    });

});

$(function () {
    $('.search-field')
        .instantSearch({
            delay: 100,
        })
        .keyup();
});