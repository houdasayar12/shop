<?php

declare(strict_types=1);

namespace App\Controller;

use App\Component\Order\Factory\OrderFactory;
use Stripe\Checkout\Session;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StripeController extends AbstractController
{
    /**
     * @Route("/commande/create-session", name="stripe_create_session")
     */
    public function index(OrderFactory $order): Response
    {
        $productsForStripe = [];
        $domain = 'http://e-commerce/';

        foreach ($order->items() as $orderItem) {
            $productsForStripe[] = [
                'price_data' => [
                    'currency' => 'eur',
                    'unit_amount' => $orderItem->getPriceTotal() * 100,
                    'product_data' => [
                        'name' => $orderItem->getProduct()->getName(),
                        'images' => [$domain.'/uploads/'.$orderItem->getProduct()->getIllustration()],
                    ],
                ],
                'quantity' => $orderItem->getQuantity(),
            ];
        }

        Stripe::setApiKey(
            'sk_test_51I3ikOE5XqBVoOIsGWzxQolOyaivJrEYi9vOqrRk2weue'.
            'WnS6Ca5nmwxQEBlS2fIRUjMWgn5CQ7P4uEPyc1B7mjD00rmhMPTsr'
        );

        $checkoutSession = Session::create([
            'customer_email' => $this->getUser()->getEmail(),
            'payment_method_types' => ['card'],
            'line_items' => [
                $productsForStripe,
            ],
            'mode' => 'payment',
            'success_url' => $domain.'/commande/merci/{CHECKOUT_SESSION_ID}',
            'cancel_url' => $domain.'/commande/erreur/{CHECKOUT_SESSION_ID}',
        ]);

        $response = new JsonResponse(['id' => $checkoutSession->id]);

        return $response;
    }
}
