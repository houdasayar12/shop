<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\ContactType;
use App\Utils\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class ContactController extends AbstractController
{
    /** @var MailerService */
    private $mailerService;

    /** @var TranslatorInterface */
    private $translator;

    public function __construct(MailerService $mailerService, TranslatorInterface $translator)
    {
        $this->mailerService = $mailerService;
        $this->translator = $translator;
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function index(Request $request): Response
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $submittedToken = $request->request->get('_token');

            if ($this->isCsrfTokenValid('contact_message', $submittedToken)) {
                $mail = $this->mailerService->create(
                    'emails/contact.html.twig',
                    $data['email'],
                    [
                        'mail' => $data['email'],
                        'message' => $data['message'],
                    ]
                );

                $this->mailerService->send($mail);
                $this->translator->trans('app.contact.default.message.success');
                $this->addFlash(
                    'info',
                    'Merci de nous avoir contacté. Notre équipe va vous répondre dans les meilleurs délais.'
                );

                return $this->redirectToRoute('contact');
            }
        }

        return $this->render('contact/contact.html.twig', ['form' => $form->createView()]);
    }
}
