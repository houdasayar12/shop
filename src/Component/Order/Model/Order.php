<?php

declare(strict_types=1);

namespace App\Component\Order\Model;

use App\Component\Core\Model\TimestampableTrait;
use App\Component\Order\Repository\OrderRepository;
use App\Component\User\Model\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order implements OrderInterface
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="guid", unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="orders")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=OrderItem::class, mappedBy="order", orphanRemoval = true, cascade={"all"})
     */
    private $items;

    /**
     * @ORM\Column(type="integer", nullable = false)
     */
    private $itemsTotal = 0;

    /**
     * @ORM\Column(type="float", nullable = false, options={"default" : 0.00})
     */
    private $itemsPriceTotal = 0;

    /**
     * @ORM\Column(type="float", nullable = false , options={"default" : 0.00})
     */
    private $priceTotal = 0;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function addItem(OrderItemInterface $item): void
    {
        $this->items->add($item);
    }

    public function removeItem(OrderItemInterface $item): void
    {
        $this->items->removeElement($item);
    }

    public function getItems(): Collection
    {
        return $this->items;
    }

    public function getItemsTotal(): int
    {
        return $this->itemsTotal;
    }

    public function setItemsTotal(int $itemsTotal): void
    {
        $this->itemsTotal = $itemsTotal;
    }

    public function getPriceTotal(): float
    {
        return $this->priceTotal;
    }

    public function setPriceTotal(float $priceTotal): void
    {
        $this->priceTotal = $priceTotal;
    }

    public function getItemsPriceTotal(): float
    {
        return $this->itemsPriceTotal;
    }

    public function setItemsPriceTotal(float $itemsPriceTotal): void
    {
        $this->itemsPriceTotal = $itemsPriceTotal;
    }

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function setUser(?UserInterface $user): void
    {
        $this->user = $user;
    }
}
