<?php

declare(strict_types=1);

namespace App\Component\Order\Factory;

use App\Component\Order\Model\OrderInterface;
use App\Component\Order\Summary;
use App\Component\Product\Model\ProductInterface;

interface OrderFactoryInterface
{
    public function createNew(): OrderInterface;

    public function getCurrent(): OrderInterface;

    public function addItem(ProductInterface $product, int $quantity): void;

    public function summary(): Summary;
}
