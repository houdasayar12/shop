<?php

declare(strict_types=1);

namespace App\Component\Address\Model;

use Symfony\Component\Security\Core\User\UserInterface;

interface AddressInterface
{
    public function getId(): ?string;

    public function getUser(): ?UserInterface;

    public function setUser(?UserInterface $user): void;

    public function getFirstName(): ?string;

    public function setFirstName(?string $firstName): void;

    public function getLastName(): ?string;

    public function setLastName(?string $lastName): void;

    public function getFullName(): string;

    public function getPhoneNumber(): ?string;

    public function setPhoneNumber(?string $phoneNumber): void;

    public function getCompany(): ?string;

    public function setCompany(?string $company): void;

    public function getCountryCode(): ?string;

    public function setCountryCode(?string $countryCode): void;

    public function getStreet(): ?string;

    public function setStreet(?string $street): void;

    public function getCity(): ?string;

    public function setCity(?string $city): void;

    public function getPostCode(): ?string;

    public function setPostCode(?string $postCode): void;
}
