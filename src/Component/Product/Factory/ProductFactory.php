<?php

declare(strict_types=1);

namespace App\Component\Product\Factory;

use App\Component\Product\Model\ProductInterface;

class ProductFactory implements ProductFactoryInterface
{
    /** @var ProductInterface */
    private $factory;

    public function __construct(ProductInterface $factory)
    {
        $this->factory = $factory;
    }

    /**
     * {@inheritdoc}
     */
    public function createNew(): ProductInterface
    {
        return new $this->factory();
    }

    public function createNamed(): void
    {
    }
}
